# arduino library loraconfig
This library ment to store all loraspecific config to the eeprom.

The libray can handle incoming characters (over Serial connection) to execute commands.

All commands must end with \r \n
The handle(char) function returns an integer that allow the caller to handle the given command.

Return Values are:

| *Constant name* | *Value* | *Decription* |
| --------------- | ------: | ------------ |
| RESULT_OK       | 0       | incoming characte is processed. nothing to do. |
| RESULT_CMD      | 1       | an internal command is executed. Resulttext is available by getMessage() |
| RESULT_UPGRADE  | 2       | command "upgrade" was recognized, can be handled by the caller |
| RESULT_ERROR    | -1      | an error occured. Read getMessage() for more info |

Commands are

| *Command*    | *Description* |
| ------------ |------------ |
| info         | Puts all infos to getMessage      |
| deveui:{hex coded deveui}| set deveui to the new value. hex coded with no spaces |
| deveui:mac | generates the deveui by the mac address of the wlan chip |
| appeui:{hex coded appeui} |  |
| appkey:{hex coded appeui} |  |
| appkey:new | generates a random appkey |
| save | stores all values to the eeprom |
| sleep-time:{time in seconds} | sets the variable for the sleeptime.  |
| reboot |  |
| config | brings the system in config mode e.g. to stop sleep |
| ssid | sets the ssid to access a wlan |
| wlan-pw | sets the password to access the wlan with already stored ssid |

/*
 * lmicHelper.h
 *
 *  Created on: 05.09.2019
 *      Author: thomas
 */

#ifndef LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_LMICHELPER_H_
#define LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_LMICHELPER_H_

#include <Arduino.h>

String get_lmic_session_info();

#endif /* LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_LMICHELPER_H_ */

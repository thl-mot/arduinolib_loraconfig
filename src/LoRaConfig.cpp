/*
 * LoRaConfig.cpp
 *
 *  Created on: 11.04.2019
 *      Author: thomas
 *
 * This Library depends on:
 *      EEPROMConfig.h  https://bitbucket.org/thl-mot/arduinolib_eepromconfig
 *		lmic.h			https://github.com/mcci-catena/arduino-lmic
 */

#include "config/hardwareconfig.h"
#include "LoRaConfig.h"
#include "EEPROMConfig.h"
#include "WiFi.h"
#include "structures.h"

#ifdef LORA_CONFIG_LMIC
#include <lmicHelper.h>
#include <arduino_lmic.h>
#endif

// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const uint8_t PROGMEM APPEUI[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00 };
// This should also be in little endian format, see above.
static const uint8_t PROGMEM DEVEUI[8] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00 };

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
// The key shown here is the semtech default key.
static const uint8_t PROGMEM APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

LoRaConfig::LoRaConfig() {
	buffer = "";
}

bool LoRaConfig::load() {
	eepromConfigIsValid = configLoad((uint8_t *) &lora_settings,
			sizeof(lora_settings), 0);
	if (!eepromConfigIsValid) {
		memcpy_P(lora_settings.APPEUI, APPEUI, 8);

		memcpy_P(lora_settings.DEVEUI, DEVEUI, 8);

		for (int i = 0; i < 16; i++) {
			lora_settings.APPKEY[i] = (int) (rand() * 255);
		}
		lora_settings.sleep_time = 60;
		lora_settings.uref = 3.3;

#ifdef LORA_CONFIG_WLAN
		memset(lora_settings.ssid, 0, sizeof(lora_settings.ssid));
		memset(lora_settings.wlan_pw, 0, sizeof(lora_settings.wlan_pw));
#endif
		save();
		return false;
	} else {
		return true;
	}
}

void LoRaConfig::save() {
	configSave((uint8_t *) &lora_settings, sizeof(lora_settings), 0);
}

LoRaConfig::~LoRaConfig() {
}

String LoRaConfig::getMessage() {
	return message;
}

bool LoRaConfig::isConfigMode() {
	// timout is 300sec = 5min
	return configMode;
}

String hexArrayToString(uint8_t* array, uint8_t size, bool reverse) {
	String r = "";
	for (int i = 0; i < size; i++) {
		uint8_t v = reverse ? array[size - i - 1] : array[i];
		if (v < 16) {
			r += "0";
		}
		r += String(v, HEX);
	}
	return r;
}

int hexStringToArray(String str, uint8_t* array, uint8_t size, bool reverse) {
	if (str.length() != size * 2) {
		return PARSER_INVALID_SIZE;
	}
	for (int i = 0; i < size; i++) {
		int8_t h = String(F("0123456789abcdef")).indexOf(str.charAt(i * 2));
		int8_t l = String(F("0123456789abcdef")).indexOf(str.charAt(i * 2 + 1));
		if (h < 0 || l < 0) {
			return PARSER_INVALID_CHARACTER;
		}
		array[reverse ? size - i - 1 : i] = h * 16 + l;
	}

	return PARSER_OK;
}

bool LoRaConfig::isValid() {
	return eepromConfigIsValid;
}

String LoRaConfig::getInfo() {
	String str="";
	str += "LoRa Region: "+ String(CFG_region)+"\n";
	str += "DEVEUI(MSB):" + hexArrayToString(lora_settings.DEVEUI, 8, true)
			+ "\n";
	str += "APPEUI(MSB):" + hexArrayToString(lora_settings.APPEUI, 8, true)
			+ "\n";
	str += "APPKEY:" + hexArrayToString(lora_settings.APPKEY, 16, false) + "\n";
	str += "sleep-time(min):" + String(lora_settings.sleep_time) + "\n";
	str += "uref(V):" + String(lora_settings.uref) + "\n";

#ifdef LORA_CONFIG_WLAN
	str += F("SSID:");
	str += String(lora_settings.ssid) + "\n";
	str += F("WLAN-PW:");
	str += String(lora_settings.wlan_pw) + "\n";
#endif

#ifdef LORA_CONFIG_LMIC
	str += get_lmic_session_info();
#endif

	return str;
}

int LoRaConfig::handleSetting(int c) {
	if (c == 13) {
		String value = "";
		message = "";
#ifdef LORA_CONFIG_WLAN
		String caseSensitive= buffer;
#endif
		buffer.toLowerCase();
		if (buffer.startsWith(F("appeui:"))) {
			value = buffer.substring(7);
			value.trim();

			int r = hexStringToArray(value, lora_settings.APPEUI, 8, true);

			if (r != PARSER_OK) {
				message = F("ERROR: APPEUI must have 8 byte");
				buffer = "";
				return RESULT_ERROR;
			}
			message = F("changed APPEUI ");
			message = message + value;
		} else if (buffer.startsWith(F("deveui:"))) {
			value = buffer.substring(7);
			value.trim();

			if (value.equals(F("mac"))) {
				uint8_t mac[6];
				WiFi.mode(WIFI_MODE_STA);
				WiFi.macAddress(mac);

				lora_settings.DEVEUI[0] = mac[0];
				lora_settings.DEVEUI[1] = mac[1];
				lora_settings.DEVEUI[2] = mac[2];
				lora_settings.DEVEUI[3] = 0xFF;
				lora_settings.DEVEUI[4] = 0xFE;
				lora_settings.DEVEUI[5] = mac[3];
				lora_settings.DEVEUI[6] = mac[4];
				lora_settings.DEVEUI[7] = mac[5];

			} else {
				int r = hexStringToArray(value, lora_settings.DEVEUI, 8, true);
				if (r != PARSER_OK) {
					message = F("ERROR: DEVEUI must have 8 byte");
					buffer = "";
					return RESULT_ERROR;
				}
			}
			message = F("changed DEVEUI ");
			message = message + hexArrayToString(lora_settings.DEVEUI, 8, true);
		} else if (buffer.startsWith(F("appkey:"))) {
			value = buffer.substring(7);
			value.trim();
			if (value.equals(F("new"))) {
				for (int i = 0; i < 16; i++) {
					lora_settings.APPKEY[i] = (int) (rand() * 255);
				}
			} else {
				int r = hexStringToArray(value, lora_settings.APPKEY, 16,
						false);
				if (r != PARSER_OK) {
					message = F("ERROR: DEVEUI must have 16 byte");
					buffer = "";
					return RESULT_ERROR;
				}
			}
			message = F("changed APPKEY ");
			message = message
					+ hexArrayToString(lora_settings.APPKEY, 16, false);
		} else if (buffer.startsWith(F("sleep-time:"))) {
			value = buffer.substring(11);
			value.trim();
			lora_settings.sleep_time = atoi(value.c_str());
			message = F("changed sleep-time ");
			message = message + String(lora_settings.sleep_time) + " min";
		} else if (buffer.startsWith(F("uref:"))) {
			value = buffer.substring(5);
			value.trim();
			lora_settings.uref = atoff(value.c_str());
			message = F("changed uref ");
			message = message + String(lora_settings.uref) + " V";
		} else if (buffer.startsWith(F("info"))) {
			message = getInfo();
		} else if (buffer.startsWith(F("reboot"))) {
			ESP.restart();
		} else if (buffer.startsWith(F("save"))) {
			message = "saved";
			save();
		} else if (buffer.startsWith(F("config"))) {
			message = "entered config mode";
			configMode = true;

#ifdef LORA_CONFIG_WLAN
		} else if (buffer.startsWith(F("ssid"))) {
			value = caseSensitive.substring(5);
			value.trim();
			message = "ssid:"+value;
			configMode = true;
			strncpy( lora_settings.ssid, value.c_str(), 32);
		} else if (buffer.startsWith(F("wlan-pw"))) {
			value = caseSensitive.substring(8);
			value.trim();
			message = "wlan-pw:"+value;
			strncpy( lora_settings.wlan_pw, value.c_str(), 32);
			configMode = true;
		} else if (buffer.startsWith(F("upgrade"))) {
			buffer="";
			return RESULT_UPGRADE;
#endif

		} else {
			message = "ERROR: unknown command " + buffer;
			buffer = "";
			return RESULT_ERROR;
		}
		buffer = "";
		return RESULT_CMD;
	} else if (c == 10) {
		message = "";
		return RESULT_OK;
	} else {
		buffer = buffer + (char) c;
		message = "";
		return RESULT_OK;
	}
}

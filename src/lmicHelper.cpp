/*
 * lmicHelper.cpp
 *
 *  Created on: 05.09.2019
 *      Author: thomas
 */

#include <lmicHelper.h>
#include <lmic.h>
#include <structures.h>
#include "LoRaConfig.h"

void os_getArtEui(u1_t* buf) {
	memcpy(buf, lora_settings.APPEUI, 8);
}

void os_getDevEui(u1_t* buf) {
	memcpy(buf, lora_settings.DEVEUI, 8);
}

void os_getDevKey(u1_t* buf) {
	memcpy(buf, lora_settings.APPKEY, 16);
}

String get_lmic_session_info() {
	String str = "";
	u4_t netid = 0;
	devaddr_t devaddr = 0;
	u1_t nwkKey[16];
	u1_t artKey[16];
	LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);

	str += F("netid: ");
	str += String(netid, DEC) + F("\n");
	str += "devaddr: ";
	str += String(devaddr, HEX) + F("\n");
	str += F("artKey: ");
	for (int i = 0; i < sizeof(artKey); ++i) {
		str += String(artKey[i], HEX);
	}
	str += F("\n");
	str += F("nwkKey: ");
	for (int i = 0; i < sizeof(nwkKey); ++i) {
		str += String(nwkKey[i], HEX);
	}
	str += +"\n";
	return str;
}



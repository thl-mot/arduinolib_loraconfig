/*
 * LoRaSettings.h
 *
 *  Created on: 05.09.2019
 *      Author: thomas
 */

#ifndef LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_STRUCTURES_H_
#define LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_STRUCTURES_H_

#include <Arduino.h>
#include "config/hardwareconfig.h"

struct LoRaConfigStruct {
	uint8_t APPEUI[8];	// LSB-first
	uint8_t DEVEUI[8];	// LSB-first
	uint8_t APPKEY[16];
	unsigned long sleep_time;
	float uref;

#ifdef LORA_CONFIG_WLAN
	char ssid[32];
	char wlan_pw[64];
#endif

};

extern LoRaConfigStruct lora_settings;

#endif /* LIBRARIES_ARDUINOLIB_LORACONFIG_SRC_STRUCTURES_H_ */

/*
 * LoRaConfig.h
 *
 *  Created on: 11.04.2019
 *      Author: thomas
 */

#ifndef LORACONFIG_H_
#define LORACONFIG_H_

#include <Arduino.h>

// Error occured
#define RESULT_ERROR -1
// Command executed, message available
#define RESULT_CMD    1
// handle upgrade
#define RESULT_UPGRADE 2
// handled incoming character, more characters expected
#define RESULT_OK     0

#define PARSER_OK 0
#define PARSER_INVALID_SIZE -1
#define PARSER_INVALID_CHARACTER -1

class LoRaConfig {
private:
	bool eepromConfigIsValid=false;
	String buffer;
	String message;
	bool configMode= false;
public:
	LoRaConfig();
	virtual ~LoRaConfig();
	String getInfo();
	bool load();
	void save();
	int handleSetting( int c);
	String getMessage();
	bool isConfigMode();
	bool isValid();
};

#endif /* LORACONFIG_H_ */
